import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { addItem, addCount, minCount } from '../reducers/reposReducer';
import { selectCount, mapStateToProps } from '../reducers/reposReducer';
import styles from './Counter.module.css';
import { openBasket } from '../script';
import { closeBasket } from '../script';
import products from "../data/dataset.json";
import { store } from "../reducers/index";
import { Basket } from './Basket';



// import { Basket } from '../../components/Basket';


export function Products() {
  const count = useSelector(selectCount);
  // const [count, setCount] = useState([]);
  const dispatch = useDispatch();

  // const count = useSelector((state) => state.items.{id}.count);
  // const [incrementAmount, setIncrementAmount] = useState('2');

  // function onCountClick() {
  //   dispatch(addItem("Hey"))
  // }
  function add() {
    dispatch(addCount())
  }
  function min() {
    dispatch(minCount())
  }

  const [incrementAmount, setIncrementAmount] = useState('2');

  const incrementValue = Number(incrementAmount) || 0;


  function byAmount(){
    dispatch(byAmount())
  }
  // const out = store.getState();
  // const out = 'fd';



  let arr = [];


  // function addToArr(){
  //   out.forEach(item => {
  //     if(item.count > 0){
  //       arr.push(item);
  //     }
  //   })
  // }

  // addToArr();

  return (
    <>
      <div>
        <p>{count}</p>
        <button onClick={() => {
          // console.log(JSON.stringify(store.getState()))
          // console.log('Update salary: ', store.getState().repos);
          // console.log(out.repos)
          console.log(count)

        }}>
          SET
        </button>
        <button onClick={() => {
          add()
          console.log({ count })
        }}>
          ADD
        </button>
        <button onClick={() => {
          min()
        }}>
          MINUS
        </button>
      </div>

      <div className="catalog">
        <div className="wrapper">
          {products.map(product => {

            // let idOut = product.id;
            // let out2 =
            //   out.map(home => {
            //     if (home.count === { idOut }) {
            //       return home.count
            //     }
            //   })

            return (
              <div key={product.id} className="grid">
                <a href={product.image}><img src={product.image} alt="" /></a>
                <div className="grid__title">
                  {product.title}
                </div>
                <div className="grid__price">
                  {product.price}$
                </div>
                <div className="grid__input">
                  <input
                    className={styles.textbox}
                    aria-label="Set increment amount"
                    value={incrementAmount}
                    onChange={(e) => setIncrementAmount(e.target.value)}
                  />
                  <button
                    className={styles.button}
                    onClick={() => dispatch(byAmount(incrementAmount))}
                  >
                    Добавити шт
                  </button>
                </div>
                <div className="grid__button">
                  <span className={styles.value}> {count} </span>
                  <button
                    className={styles.button}
                    aria-label="Increment value"
                    onClick={() => dispatch(addCount(product.id))}
                  >
                    Добавити
                  </button>
                </div>
              </div>
            )
          })}
        </div>
      </div>
      <Basket count={2} open={openBasket} close={closeBasket} title={"dssdsd"} add={() => dispatch(addCount())} min={() => dispatch(dispatch(minCount()))} image={products[1].image} arr={arr} />
    </>
  )
}


// export function Products(){
//   const dispatch = useDispatch();
//   const count = useSelector(selectCount);


  // // function onCountClick(){
  // //   dispatch(addItem("Hey"))
  // // }

  // function onCountClick(){
  //   dispatch(addItem("Hey"))
  // }
  // function add(){
  //   dispatch(addCount())
  // }
  // function min(){
  //   dispatch(minCount())
  // }

  // return (
  //   <div>
  //       <p>{count}</p>
  //       <button onClick={() => {
  //         onCountClick()
  //       }}>
  //         SET
  //       </button>
  //       <button onClick={() => {
  //         add()
  //       }}>
  //         ADD
  //       </button>
  //       <button onClick={() => {
  //         min()
  //       }}>
  //         MINUS
  //       </button>
  //   </div>
  // )
// };


