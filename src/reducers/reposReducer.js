import products from "../data/dataset.json";


const ADD_ITEM = "ADD_ITEM";
const ADD_COUNT = "ADD_COUNT";
const MIN_COUNT = "MIN_COUNT";
const INIT = "@@INIT";
const BY_AMOUNT = "BY_AMOUNT";



const defaultState = {
    // items: [],
    // isFetching: true,
}

// const rar = {
//     name: "Vika",
//     surname: "Prokop"

// }
// const ra3r = {
//     name: "Vika",
//     surname: "Prokop"
// }
// const ra32r = {
//     name: "Vika",
//     surname: "Prokop"
// }



export default function reposReducer(state = defaultState, action) {
    switch (action.type) {
        // case ADD_ITEM:
        //     // state.items.push(rar,ra3r,ra32r)
        //     products.forEach(item => {
        //         return {

        //             // ...state,
        //             // items: {
        //             //     rar,
        //             //     ra3r,
        //             //     ra32r
        //             // }
        //         };   
        //     });
        //     return {

        //         // ...state,
        //         // items: {
        //         //     rar,
        //         //     ra3r,
        //         //     ra32r
        //         // }
        //     };
        case INIT:
            return {
                ...state,
                ...state.items = products,
            };
        case ADD_COUNT:
            products.forEach(item => {
                if (item.id === action.payload) {
                    item.count += 1;
                    return {
                        item
                    }
                }
            })
            return {
                ...state,
                ...state.items = products
            }

        case MIN_COUNT: {
            products.forEach(item => {
                if (item.id === action.payload) {
                    item.count -= 1;
                    return {
                        item
                    }
                }
            })
            return {
                ...state,
                ...state.items = products
            }
        }
        case BY_AMOUNT: {
            products.forEach(item => {
                if (item.id === action.id) {
                    item.count += action.payload;;
                    return {
                        item
                    }
                }
            })
            return {
                ...state,
                ...state.items = products
            }
        }

        default: {
            return state;
        }
    }
}


export const selectCount = (state) => state.repos[0].count;


export const mapStateToProps = state => state;

// export function selectCount(id) {
//     // eslint-disable-next-line no-unused-expressions
//     (state) => {
//         let out = state.repos[id].count;
//         return out;
//     }
// }

// export const addCount = (count) => ({ type: ADD_COUNT, payload: id });
export function addCount(id) {
    let out = { type: ADD_COUNT, payload: id };
    return out;
}

export function minCount(id) {
    let out = { type: MIN_COUNT, payload: id };
    return out;
}

export function byAmount(id, count) {
    let out = { type: BY_AMOUNT, payload: count };
    return out;
}


// export const minCount = (count) => ({ type: MIN_COUNT, payload: count -= 1 });


export const addItem = (item) => ({ type: ADD_ITEM, payload: item });


